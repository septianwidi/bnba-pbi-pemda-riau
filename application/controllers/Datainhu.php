<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datainhu extends CI_Controller {
	function __construct(){
	 parent::__construct();
	 	//validasi jika user belum login
     $this->data['CI'] =& get_instance();
     $this->load->helper(array('form', 'url'));
     $this->load->model('M_Admin');
		if($this->session->userdata('masuk_bnba') != TRUE){
				$url=base_url('login');
				redirect($url);
		}
	}

	public function index()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$this->data['pbiinhu'] =  $this->db->query("SELECT * FROM tbl_pbiinhu ORDER BY nama_peserta ASC");
        $this->data['title_web'] = 'Data Peserta PBI Kab. Indragiri Hulu';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
		$this->load->view('pbiinhu/pbiinhu_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function print()
    {	
		if($this->session->userdata('level') == 'Admin'){
			if($this->uri->segment('3') == ''){ echo '<script>alert("halaman tidak ditemukan");window.location="'.base_url('datainhu').'";</script>';}
			$this->data['idbo'] = $this->session->userdata('ses_id');
			$count = $this->M_Admin->CountTableId('tbl_pbiinhu','no_bpjs',$this->uri->segment('3'));
			if($count > 0)
			{			
				$this->data['pbiinhu'] = $this->M_Admin->get_tableid_edit('tbl_pbiinhu','no_bpjs',$this->uri->segment('3'));
				$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();
			}else{
				echo '<script>alert("USER TIDAK DITEMUKAN");window.location="'.base_url('datainhu').'"</script>';
			}		
		}elseif($this->session->userdata('level') == 'Petugas'){
			$this->data['idbo'] = $this->session->userdata('ses_id');
			$count = $this->M_Admin->CountTableId('tbl_pbiinhu','no_bpjs',$this->session->userdata('ses_id'));
			if($count > 0)
			{			
				$this->data['pbiinhu'] = $this->M_Admin->get_tableid_edit('tbl_pbiinhu','no_bpjs',$this->session->userdata('ses_id'));
			}else{
				echo '<script>alert("USER TIDAK DITEMUKAN");window.location="'.base_url('datainhu').'"</script>';
			}
		}
        $this->data['title_web'] = 'Print Kartu Peserta PBI';
        $this->load->view('pbiinhu/print',$this->data);
    }

	public function pbiinhudetail()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$count = $this->M_Admin->CountTableId('tbl_pbiinhu','no_bpjs',$this->uri->segment('3'));
		if($count > 0)
		{
			$this->data['pbiinhu'] = $this->M_Admin->get_tableid_edit('tbl_pbiinhu','no_bpjs',$this->uri->segment('3'));
			

		}else{
			echo '<script>alert("DATA TIDAK DITEMUKAN");window.location="'.base_url('datainhu').'"</script>';
		}

		$this->data['title_web'] = 'Data Peserta Detail';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('pbiinhu/detail',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function pbiinhuedit()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$count = $this->M_Admin->CountTableId('tbl_pbiinhu','no_bpjs',$this->uri->segment('3'));
		if($count > 0)
		{
			$this->data['pbiinhu'] = $this->M_Admin->get_tableid_edit('tbl_pbiinhu','no_bpjs',$this->uri->segment('3'));	
			$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();
		}else{
			echo '<script>alert("DATA TIDAK DITEMUKAN");window.location="'.base_url('datainhu').'"</script>';
		}

		$this->data['title_web'] = 'Edit Data Peserta';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('pbiinhu/edit_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function pbiinhutambah()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();

        $this->data['title_web'] = 'Tambah Peserta';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('pbiinhu/tambah_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function prosespbiinhu()
	{
		if($this->session->userdata('masuk_bnba') != TRUE){
			$url=base_url('login');
			redirect($url);
		}
		// hapus aksi form proses pbiinhu
		if(!empty($this->input->get('nama_peserta')))
		{
        
			$pbiinhu = $this->M_Admin->get_tableid_edit('tbl_pbiinhu','no_bpjs',htmlentities($this->input->get('nama_peserta')));
			
			$this->M_Admin->delete_table('tbl_pbiinhu','no_bpjs',$this->input->get('nama_peserta'));
			
			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-warning">
					<p> Berhasil Hapus Data Peserta !</p>
				</div></div>');
			redirect(base_url('datainhu'));  
		}

		// tambah aksi form proses pbiinhu
		if(!empty($this->input->post('tambah')))
		{
			$post= $this->input->post();
			$data = array(
				'no_bpjs'=> htmlentities($post['no_bpjs']),
				'nik' => htmlentities($post['nik']), 
				'nama_peserta'  => htmlentities($post['nama_peserta']), 
				'kode_faskes'=> htmlentities($post['faskespilih']),  
				'alamat'  => htmlentities($post['alamat']),
				'status_peserta'=> htmlentities($post['status_peserta']),     
				
			);

			$this->db->insert('tbl_pbiinhu', $data);

			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
			<p> Tambah Peserta Sukses !</p>
			</div></div>');
			redirect(base_url('datainhu')); 
		}

		// edit aksi form proses pbiinhu
		if(!empty($this->input->post('edit')))
		{
			$post = $this->input->post();
			$data = array(
				 
				'nik' => htmlentities($post['nik']), 
				'nama_peserta' => htmlentities($post['nama_peserta']), 
				'kode_faskes'=> htmlentities($post['faskespilih']), 
				'alamat'  => htmlentities($post['alamat']),
				'status_peserta'=> htmlentities($post['status_peserta']) 
			);

			$this->db->where('no_bpjs',htmlentities($post['edit']));
			$this->db->update('tbl_pbiinhu', $data);

			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
					<p> Edit Data Peserta Sukses !</p>
				</div></div>');
			redirect(base_url('datainhu/pbiinhuedit/'.$post['edit'])); 
		}

	}

}
