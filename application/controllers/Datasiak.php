<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datasiak extends CI_Controller {
	function __construct(){
	 parent::__construct();
	 	//validasi jika user belum login
     $this->data['CI'] =& get_instance();
     $this->load->helper(array('form', 'url'));
     $this->load->model('M_Admin');
		if($this->session->userdata('masuk_bnba') != TRUE){
				$url=base_url('login');
				redirect($url);
		}
	}

	public function index()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$this->data['pbisiak'] =  $this->db->query("SELECT * FROM tbl_pbisiak ORDER BY nama_peserta ASC");
		$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();
        $this->data['title_web'] = 'Data Peserta PBI Kab. Siak';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
		$this->load->view('pbisiak/pbisiak_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function print()
    {	
		if($this->session->userdata('level') == 'Admin'){
			if($this->uri->segment('3') == ''){ echo '<script>alert("halaman tidak ditemukan");window.location="'.base_url('datasiak').'";</script>';}
			$this->data['idbo'] = $this->session->userdata('ses_id');
			$count = $this->M_Admin->CountTableId('tbl_pbisiak','no_bpjs',$this->uri->segment('3'));
			if($count > 0)
			{			
				$this->data['pbisiak'] = $this->M_Admin->get_tableid_edit('tbl_pbisiak','no_bpjs',$this->uri->segment('3'));
				$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();
			}else{
				echo '<script>alert("USER TIDAK DITEMUKAN");window.location="'.base_url('datasiak').'"</script>';
			}		
		}elseif($this->session->userdata('level') == 'Petugas'){
			$this->data['idbo'] = $this->session->userdata('ses_id');
			$count = $this->M_Admin->CountTableId('tbl_pbisiak','no_bpjs',$this->session->userdata('ses_id'));
			if($count > 0)
			{			
				$this->data['pbisiak'] = $this->M_Admin->get_tableid_edit('tbl_pbisiak','no_bpjs',$this->session->userdata('ses_id'));
			}else{
				echo '<script>alert("USER TIDAK DITEMUKAN");window.location="'.base_url('datasiak').'"</script>';
			}
		}
        $this->data['title_web'] = 'Print Kartu Peserta PBI';
        $this->load->view('pbisiak/print',$this->data);
    }

	public function pbisiakdetail()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$count = $this->M_Admin->CountTableId('tbl_pbisiak','no_bpjs',$this->uri->segment('3'));
		if($count > 0)
		{
			$this->data['pbisiak'] = $this->M_Admin->get_tableid_edit('tbl_pbisiak','no_bpjs',$this->uri->segment('3'));
			$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();

		}else{
			echo '<script>alert("DATA TIDAK DITEMUKAN");window.location="'.base_url('datasiak').'"</script>';
		}

		$this->data['title_web'] = 'Data Peserta Detail';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('pbisiak/detail',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function pbisiakedit()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$count = $this->M_Admin->CountTableId('tbl_pbisiak','no_bpjs',$this->uri->segment('3'));
		if($count > 0)
		{
			$this->data['pbisiak'] = $this->M_Admin->get_tableid_edit('tbl_pbisiak','no_bpjs',$this->uri->segment('3'));
			$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();	
		}else{
			echo '<script>alert("DATA TIDAK DITEMUKAN");window.location="'.base_url('datasiak').'"</script>';
		}

		$this->data['title_web'] = 'Edit Data Peserta';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('pbisiak/edit_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function pbisiaktambah()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY kode_faskes DESC")->result_array();

        $this->data['title_web'] = 'Tambah Peserta';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('pbisiak/tambah_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function prosespbisiak()
	{
		if($this->session->userdata('masuk_bnba') != TRUE){
			$url=base_url('login');
			redirect($url);
		}

		// print aksi form proses pbisiak
		if(!empty($this->input->get('print')))
		{
			$get= $this->input->get();
			$data = array(
				'no_bpjs'=> htmlentities($get['no_bpjs']),
				'nik' => htmlentities($get['nik']), 
				'nama_peserta'  => htmlentities($get['nama_peserta']), 
				'nama_faskes'=> htmlentities($get['nama_faskes']), 
				'alamat'  => htmlentities($get['alamat']),
				'status_peserta'=> htmlentities($get['status_peserta']),     
				
			);

			$this->db->insert('tbl_pbisiak', $data);

			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
			<p> Tambah Peserta Sukses !</p>
			</div></div>');
			redirect(base_url('datasiak')); 
		}

		// hapus aksi form proses pbisiak
		if(!empty($this->input->get('nama_peserta')))
		{
        
			$pbisiak = $this->M_Admin->get_tableid_edit('tbl_pbisiak','no_bpjs',htmlentities($this->input->get('nama_peserta')));
			
			$this->M_Admin->delete_table('tbl_pbisiak','no_bpjs',$this->input->get('nama_peserta'));
			
			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-warning">
					<p> Berhasil Hapus Data Peserta !</p>
				</div></div>');
			redirect(base_url('datasiak'));  
		}

		// tambah aksi form proses pbisiak
		if(!empty($this->input->post('tambah')))
		{
			$post= $this->input->post();
			$data = array(
				'no_bpjs'=> htmlentities($post['no_bpjs']),
				'nik' => htmlentities($post['nik']), 
				'nama_peserta'  => htmlentities($post['nama_peserta']), 
				'kode_faskes'=> htmlentities($post['faskespilih']), 
				'alamat'  => htmlentities($post['alamat']),
				'status_peserta'=> htmlentities($post['status_peserta']),     
				
			);

			$this->db->insert('tbl_pbisiak', $data);

			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
			<p> Tambah Peserta Sukses !</p>
			</div></div>');
			redirect(base_url('datasiak')); 
		}

		// edit aksi form proses pbisiak
		if(!empty($this->input->post('edit')))
		{
			$post = $this->input->post();
			$data = array(
				 
				'nik' => htmlentities($post['nik']), 
				'nama_peserta' => htmlentities($post['nama_peserta']), 
				'kode_faskes'  => htmlentities($post['faskespilih']),
				'alamat'  => htmlentities($post['alamat']),
				'status_peserta'=> htmlentities($post['status_peserta']) 
			);

			$this->db->where('no_bpjs',htmlentities($post['edit']));
			$this->db->update('tbl_pbisiak', $data);

			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
					<p> Edit Data Peserta Sukses !</p>
				</div></div>');
			redirect(base_url('datasiak/pbisiakedit/'.$post['edit'])); 
		}

	}

}
