<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
	 parent::__construct();
	 	//validasi jika user belum login
     $this->data['CI'] =& get_instance();
     $this->load->helper(array('form', 'url'));
     $this->load->model('M_Admin');
	 	 if($this->session->userdata('masuk_bnba') != TRUE){
				 $url=base_url('login');
				 redirect($url);
		 }
	 }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$this->data['title_web'] = 'Dashboard ';
		$this->data['count_pengguna']=$this->db->query("SELECT * FROM tbl_login")->num_rows();
		$this->data['count_pbikuansingaktif']=$this->db->query("SELECT * FROM tbl_pbikuansing WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbikuansingtidakaktif']=$this->db->query("SELECT * FROM tbl_pbikuansing WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbiinhuaktif']=$this->db->query("SELECT * FROM tbl_pbiinhu WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbiinhutidakaktif']=$this->db->query("SELECT * FROM tbl_pbiinhu WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbiinhilaktif']=$this->db->query("SELECT * FROM tbl_pbiinhil WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbiinhiltidakaktif']=$this->db->query("SELECT * FROM tbl_pbiinhil WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbipelalawanaktif']=$this->db->query("SELECT * FROM tbl_pbipelalawan WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbipelalawantidakaktif']=$this->db->query("SELECT * FROM tbl_pbipelalawan WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbisiakaktif']=$this->db->query("SELECT * FROM tbl_pbisiak WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbisiaktidakaktif']=$this->db->query("SELECT * FROM tbl_pbisiak WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbikamparaktif']=$this->db->query("SELECT * FROM tbl_pbikampar WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbikampartidakaktif']=$this->db->query("SELECT * FROM tbl_pbikampar WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbirohulaktif']=$this->db->query("SELECT * FROM tbl_pbirohul WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbirohultidakaktif']=$this->db->query("SELECT * FROM tbl_pbirohul WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbibengkalisaktif']=$this->db->query("SELECT * FROM tbl_pbibengkalis WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbibengkalistidakaktif']=$this->db->query("SELECT * FROM tbl_pbibengkalis WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbirohilaktif']=$this->db->query("SELECT * FROM tbl_pbirohil WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbirohiltidakaktif']=$this->db->query("SELECT * FROM tbl_pbirohil WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbimerantiaktif']=$this->db->query("SELECT * FROM tbl_pbimeranti WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbimerantitidakaktif']=$this->db->query("SELECT * FROM tbl_pbimeranti WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbipkuaktif']=$this->db->query("SELECT * FROM tbl_pbipku WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbipkutidakaktif']=$this->db->query("SELECT * FROM tbl_pbipku WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->data['count_pbidumaiaktif']=$this->db->query("SELECT * FROM tbl_pbidumai WHERE status_peserta='AKTIF'")->num_rows();
		$this->data['count_pbidumaitidakaktif']=$this->db->query("SELECT * FROM tbl_pbidumai WHERE status_peserta='TIDAK AKTIF'")->num_rows();
		$this->load->view('header_view',$this->data);
		$this->load->view('sidebar_view',$this->data);
		$this->load->view('dashboard_view',$this->data);
		$this->load->view('footer_view',$this->data);
	}

}
