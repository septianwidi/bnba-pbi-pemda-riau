<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datafaskes extends CI_Controller {
	function __construct(){
	 parent::__construct();
	 	//validasi jika user belum login
     $this->data['CI'] =& get_instance();
     $this->load->helper(array('form', 'url'));
     $this->load->model('M_Admin');
		if($this->session->userdata('masuk_bnba') != TRUE){
				$url=base_url('login');
				redirect($url);
		}
	}

	public function index()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$this->data['faskes'] =  $this->db->query("SELECT * FROM tbl_faskes ORDER BY nama_faskes ASC");
        $this->data['title_web'] = 'Data Fasilitas Kesehatan';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
		$this->load->view('faskes/faskes_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function faskesdetail()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$count = $this->M_Admin->CountTableId('tbl_faskes','kode_faskes',$this->uri->segment('3'));
		if($count > 0)
		{
			$this->data['faskes'] = $this->M_Admin->get_tableid_edit('tbl_faskes','kode_faskes',$this->uri->segment('3'));
			

		}else{
			echo '<script>alert("DATA TIDAK DITEMUKAN");window.location="'.base_url('datafaskes').'"</script>';
		}

		$this->data['title_web'] = 'Data Peserta Detail';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('faskes/detail',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function faskesedit()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
		$count = $this->M_Admin->CountTableId('tbl_faskes','kode_faskes',$this->uri->segment('3'));
		if($count > 0)
		{
			$this->data['faskes'] = $this->M_Admin->get_tableid_edit('tbl_faskes','kode_faskes',$this->uri->segment('3'));	
		}else{
			echo '<script>alert("DATA TIDAK DITEMUKAN");window.location="'.base_url('datafaskes').'"</script>';
		}

		$this->data['title_web'] = 'Edit Data Peserta';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('faskes/edit_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function faskestambah()
	{
		$this->data['idbo'] = $this->session->userdata('ses_id');
        $this->data['title_web'] = 'Tambah Fasilitas Kesehatan';
        $this->load->view('header_view',$this->data);
        $this->load->view('sidebar_view',$this->data);
        $this->load->view('faskes/tambah_view',$this->data);
        $this->load->view('footer_view',$this->data);
	}

	public function prosesfaskes()
	{
		if($this->session->userdata('masuk_bnba') != TRUE){
			$url=base_url('login');
			redirect($url);
		}
		// hapus aksi form proses faskes
		if(!empty($this->input->get('nama_faskes')))
		{
        
			$faskes = $this->M_Admin->get_tableid_edit('tbl_faskes','kode_faskes',htmlentities($this->input->get('nama_faskes')));
			
			$this->M_Admin->delete_table('tbl_faskes','kode_faskes',$this->input->get('nama_faskes'));
			
			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-warning">
					<p> Berhasil Hapus Data Faskes!</p>
				</div></div>');
			redirect(base_url('datafaskes'));  
		}

		// tambah aksi form proses faskes
		if(!empty($this->input->post('tambah')))
		{
			$post= $this->input->post();
			$data = array(
				'kode_faskes'=> htmlentities($post['kode_faskes']),
				'nama_faskes'  => htmlentities($post['nama_faskes']),    
				
			);

			$this->db->insert('tbl_faskes', $data);

			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
			<p> Tambah Fasilitas Kesehatan Sukses !</p>
			</div></div>');
			redirect(base_url('datafaskes')); 
		}

		// edit aksi form proses faskes
		if(!empty($this->input->post('edit')))
		{
			$post = $this->input->post();
			$data = array(
				 
				'nama_faskes' => htmlentities($post['nama_faskes'])
			);

			$this->db->where('kode_faskes',htmlentities($post['edit']));
			$this->db->update('tbl_faskes', $data);

			$this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
					<p> Edit Data Peserta Sukses !</p>
				</div></div>');
			redirect(base_url('datafaskes/faskesedit/'.$post['edit'])); 
		}

	}

}
