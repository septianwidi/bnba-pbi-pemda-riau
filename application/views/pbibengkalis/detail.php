<?php if(! defined('BASEPATH')) exit('No direct script acess allowed');?>
<?php
	$kode_faskes = $pbibengkalis->kode_faskes;

	$faskes = $this->M_Admin->get_tableid_edit('tbl_faskes','kode_faskes',$kode_faskes);

?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-book" style="color:green"> </i>  <?= $title_web;?>
    </h1>
    <ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
			<li class="active"><i class="fa fa-book"></i>&nbsp;  <?= $title_web;?></li>
    </ol>
  </section>
  <section class="content">
	<div class="row">
	    <div class="col-md-12">
	        <div class="box box-primary">
                <div class="box-header with-border">
					<h4><?= $pbibengkalis->nama_peserta;?></h4>
                </div>
			    <!-- /.box-header -->
			    <div class="box-body">
					<table class="table table-striped table-bordered">
						<tr>
							<td style="width:20%">No BPJS</td>
							<td><?= $pbibengkalis->no_bpjs;?></td>
						</tr>
						
						<tr>
							<td>NIK</td>
							<td><?= $pbibengkalis->nik;?></td>
						</tr>
						
						<tr>
							<td>Nama Peserta</td>
							<td><?= $pbibengkalis->nama_peserta;?></td>
						</tr>
						<tr>
							<td>Nama Faskes</td>
							<td><?= $faskes->nama_faskes;?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td><?= $pbibengkalis->alamat;?></td>
						</tr>
						<tr>
							<td>Status Peserta</td>
							<td><?= $pbibengkalis->status_peserta;?></td>
						</tr>
						
					</table>
					<a href="<?= base_url('databengkalis');?>" class="btn btn-danger btn-md">Kembali</a>
		        </div>
	        </div>
	    </div>
    </div>
</section>
</div>

<div class="modal-footer">
	
	
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
