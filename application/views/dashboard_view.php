<?php defined('BASEPATH') OR exit('No direct script access allowed');?>


<!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Home  <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
      <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
              <div class="col-sm-12">

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-purple">
                    <div class="inner">
                    <h3>PBI Kuantan Singingi</h3>
                    <p>Peserta Aktif : <?= $count_pbikuansingaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbikuansingtidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datakuansing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>PBI Indragiri Hulu</h3>
                    <p>Peserta Aktif : <?= $count_pbiinhuaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbiinhutidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datainhu" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-purple">
                    <div class="inner">
                    <h3>PBI Indragiri Hilir</h3>
                    <p>Peserta Aktif : <?= $count_pbiinhilaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbiinhiltidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datainhil" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>PBI Pelalawan</h3>
                    <p>Peserta Aktif : <?= $count_pbipelalawanaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbipelalawantidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datapelalawan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-purple">
                    <div class="inner">
                    <h3>PBI Siak</h3>
                    <p>Peserta Aktif : <?= $count_pbisiakaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbisiaktidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datasiak" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>PBI Kampar</h3>
                    <p>Peserta Aktif : <?= $count_pbikamparaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbikampartidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datakampar" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-purple">
                    <div class="inner">
                    <h3>PBI Rokan Hulu</h3>
                    <p>Peserta Aktif : <?= $count_pbirohulaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbirohultidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datarohul" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>PBI Bengkalis</h3>
                    <p>Peserta Aktif : <?= $count_pbibengkalisaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbibengkalistidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="databengkalis" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-purple">
                    <div class="inner">
                    <h3>PBI Rokan Hilir</h3>
                    <p>Peserta Aktif : <?= $count_pbirohilaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbirohiltidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datarohil" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>PBI Kep. Meranti </h3>
                    <p>Peserta Aktif : <?= $count_pbimerantiaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbimerantitidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datameranti" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-purple">
                    <div class="inner">
                    <h3>PBI Pekanbaru</h3>
                    <p>Peserta Aktif : <?= $count_pbipkuaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbipkutidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datapku" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 

                <div class="col-lg-3 col-xs-6">
                   <!--small box-->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>PBI Dumai</h3>
                    <p>Peserta Aktif : <?= $count_pbidumaiaktif;?></p><p>Peserta Tidak Aktif : <?= $count_pbidumaitidakaktif;?></p>
                      
                    </div>
                    <div class="icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="datadumai" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div> 
              </div>
            </div>
        </section>
    </div>
    <!-- /.content -->
