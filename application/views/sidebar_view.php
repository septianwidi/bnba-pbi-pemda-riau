<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <?php
            $d = $this->db->query("SELECT * FROM tbl_login WHERE id_login='$idbo'")->row();
            if(isset($d->foto)){
          ?>
          <br/>
          <img src="<?php echo base_url();?>assets_style/image/<?php echo $d->foto;?>" alt="#" c
          lass="user-image" style="border:2px solid #fff;height:auto;width:100%;"/>
          <?php }else{?>
            <!--<img src="" alt="#" class="user-image" style="border:2px solid #fff;"/>-->
            <i class="fa fa-user fa-4x" style="color:#fff;"></i>
          <?php }?>
        </div>
        <div class="pull-left info" style="margin-top: 5px;">
          <p><?php echo $d->nama;?></p>
          <p><?= $d->level;?>
          </p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
		</div>
        <ul class="sidebar-menu" data-widget="tree">
			<?php if($this->session->userdata('level') == 'Admin'){?>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?php if($this->uri->uri_string() == 'dashboard'){ echo 'active';}?>">
                <a href="<?php echo base_url('dashboard');?>">
                    <i class="fa fa-home"></i> <span>Home</span>
                </a>
            </li>
            
			<li class="treeview <?php if($this->uri->uri_string() == 'data/pbidumai'){ echo 'active';}?>
				<?php if($this->uri->uri_string() == 'data'){ echo 'active';}?>
				<?php if($this->uri->uri_string() == 'data/pbidumaitambah'){ echo 'active';}?>
        <?php if($this->uri->uri_string() == 'data/pbidumaidetail/'.$this->uri->segment('3')){ echo 'active';}?>
        <?php if($this->uri->uri_string() == 'data/pbidumaiedit/'.$this->uri->segment('3')){ echo 'active';}?>">
        
                <a href="#">
                    <i class="fa fa-folder-open-o" aria-hidden="true"></i>
                    <span>Data BNBA</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        <li class="<?php if($this->uri->uri_string() == 'datakuansing'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datakuansing/pbikuansingtambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datakuansing/pbikuansingdetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datakuansing/pbikuansingedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datakuansing");?>" class="cursor">
                            <span class="fa fa-id-card" aria-hidden="true"></span> PBI Kuantan Singingi
                            
                        </a>
                    </li>

                    <li class="<?php if($this->uri->uri_string() == 'datainhu'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datainhu/pbiinhutambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datainhu/pbiinhudetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datainhu/pbiinhuedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datainhu");?>" class="cursor">
                            <span class="fa fa-id-card-o" aria-hidden="true"></span> PBI Indragiri Hulu
                            
                        </a>
                    </li>

                    <li class="<?php if($this->uri->uri_string() == 'datainhil'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datainhil/pbiinhiltambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datainhil/pbiinhildetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datainhil/pbiinhiledit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datainhil");?>" class="cursor">
                        <span class="fa fa-id-card" aria-hidden="true"></span> PBI Indragiri Hilir
                            
                        </a>
                    </li>

                    <li class="<?php if($this->uri->uri_string() == 'datapelalawan'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datapelalawan/pbipelalawantambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datapelalawan/pbipelalawandetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datapelalawan/pbipelalawanedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datapelalawan");?>" class="cursor">
                        <span class="fa fa-id-card-o" aria-hidden="true"></span> PBI Pelalawan
                            
                        </a>
                    </li>

                    <li class="<?php if($this->uri->uri_string() == 'datasiak'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datasiak/pbisiaktambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datasiak/pbisiakdetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datasiak/pbisiakedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datasiak");?>" class="cursor">
                        <span class="fa fa-id-card" aria-hidden="true"></span> PBI Siak
                            
                        </a>
                    </li>

                    <li class="<?php if($this->uri->uri_string() == 'datakampar'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datakampar/pbikampartambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datakampar/pbikampardetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datakampar/pbikamparedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datakampar");?>" class="cursor">
                        <span class="fa fa-id-card-o" aria-hidden="true"></span> PBI Kampar
                            
                        </a>
                    </li>

                    <li class="<?php if($this->uri->uri_string() == 'datarohul'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datarohul/pbirohultambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datarohul/pbirohuldetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datarohul/pbirohuledit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datarohul");?>" class="cursor">
                        <span class="fa fa-id-card" aria-hidden="true"></span> PBI Rokan Hulu
                            
                        </a>
                    </li>

                   <li class="<?php if($this->uri->uri_string() == 'databengkalis'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'databengkalis/pbibengkalistambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'databengkalis/pbibengkalisdetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'databengkalis/pbibengkalisedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("databengkalis");?>" class="cursor">
                        <span class="fa fa-id-card-o" aria-hidden="true"></span> PBI Bengkalis
                            
                        </a>
                    </li>  
                    
                   <li class="<?php if($this->uri->uri_string() == 'datarohil'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datarohil/pbirohiltambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datarohil/pbirohildetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datarohil/pbirohiledit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datarohil");?>" class="cursor">
                        <span class="fa fa-id-card" aria-hidden="true"></span> PBI Rokan Hilir
                            
                        </a>
                    </li>
                    
                    <li class="<?php if($this->uri->uri_string() == 'datameranti'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datameranti/pbimerantitambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datameranti/pbimerantidetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datameranti/pbimerantiedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datameranti");?>" class="cursor">
                        <span class="fa fa-id-card-o" aria-hidden="true"></span> PBI Kep. Meranti
                            
                        </a>
                    </li>
                    
                    <li class="<?php if($this->uri->uri_string() == 'datapku'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datapku/pbipkutambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datapku/pbipkudetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datapku/pbipkuedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datapku");?>" class="cursor">
                        <span class="fa fa-id-card" aria-hidden="true"></span> PBI Pekanbaru
                            
                        </a>
                    </li>
                    
                    <li class="<?php if($this->uri->uri_string() == 'datadumai'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datadumai/pbidumaitambah'){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datadumai/pbidumaidetail/'.$this->uri->segment('3')){ echo 'active';}?>
                        <?php if($this->uri->uri_string() == 'datadumai/pbidumaiedit/'.$this->uri->segment('3')){ echo 'active';}?>">
                        <a href="<?php echo base_url("datadumai");?>" class="cursor">
                        <span class="fa fa-id-card-o" aria-hidden="true"></span> PBI Dumai
                            
                        </a>
                    </li>  

                </ul>
            </li>
          
            <li class="<?php if($this->uri->uri_string() == 'datafaskes'){ echo 'active';}?>
                <?php if($this->uri->uri_string() == 'datafaskes/tambah'){ echo 'active';}?>
                <?php if($this->uri->uri_string() == 'datafaskes/edit/'.$this->uri->segment('3')){ echo 'active';}?>">
                <a href="<?php echo base_url('datafaskes');?>" class="cursor">
                <i class="fa fa-hospital-o"></i> <span>Fasilitas Kesehatan Tk.I</span></a>
			</li>

            <li class="<?php if($this->uri->uri_string() == 'user'){ echo 'active';}?>
                <?php if($this->uri->uri_string() == 'user/tambah'){ echo 'active';}?>
                <?php if($this->uri->uri_string() == 'user/edit/'.$this->uri->segment('3')){ echo 'active';}?>">
                <a href="<?php echo base_url('user');?>" class="cursor">
                    <i class="fa fa-user"></i> <span>Pengguna</span></a>
			</li>

			<?php }?>
			
        </ul>
        <div class="clearfix"></div>
        <br/>
        <br/>
    </section>
    <!-- /.sidebar -->
  </aside>
