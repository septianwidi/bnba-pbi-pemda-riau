<?php if(! defined('BASEPATH')) exit('No direct script acess allowed');?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-edit" style="color:green"> </i>  <?= $title_web;?>
    </h1>
    <ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
			<li class="active"><i class="fa fa-hospital-o"></i>&nbsp; <?= $title_web;?></li>
    </ol>
  </section>
  <section class="content">
	<?php if(!empty($this->session->flashdata())){ echo $this->session->flashdata('pesan');}?>
	<div class="row">
	    <div class="col-md-12">
	        <div class="box box-primary">
                <div class="box-header with-border">
					
                    <a href="datafaskes/faskestambah"><button class="btn btn-primary">
						<i class="fa fa-plus"> </i> Tambah Fasilitas Kesehatan</button></a>
					
                </div>
				<!-- /.box-header -->
				<div class="box-body">
                    <br/>
					<div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>    
                                <th>Kode Fasilitas Kesehatan</th>
                                <th>Nama Fasilitas Kesehatan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1;foreach($faskes->result_array() as $isi){?>
                            <tr>
                                <td><?= $no;?></td>
                                <td><?= $isi['kode_faskes'];?></td>
                                <td><?= $isi['nama_faskes'];?></td>								
                                <td>
								
									
									<a href="<?= base_url('datafaskes/faskesedit/'.$isi['kode_faskes']);?>"><button class="btn btn-success"><i class="fa fa-edit"></i></button></a>
									<?php if($this->session->userdata('level') == 'Admin'){?>
                                    <a href="<?= base_url('datafaskes/faskesdetail/'.$isi['kode_faskes']);?>">
									<button class="btn btn-info"><i class="fa fa-info" aria-hidden="true"></i> Detail</button></a>
                                    <a href="<?= base_url('datafaskes/prosesfaskes?nama_faskes='.$isi['kode_faskes']);?>" onclick="return confirm('Anda yakin untuk data peserta ini akan dihapus ?');">
									<button class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
									<?php }else{?>
										<a href="<?= base_url('datafaskes/faskesdetail/'.$isi['kode_faskes']);?>">
										<button class="btn btn-info"><i class="fa fa-info" aria-hidden="true"></i> Detail</button></a>
									<?php }?>
                                </td>
                            </tr>
                        <?php $no++;}?>
                        </tbody>
                    </table>
			    </div>
			    </div>
	        </div>
    	</div>
    </div>
</section>
</div>
