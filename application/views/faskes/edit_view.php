<?php if(! defined('BASEPATH')) exit('No direct script acess allowed');?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-edit" style="color:green"> </i>Update Faskes - <?= $faskes->nama_faskes;?>
    </h1>
    <ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
			<li class="active"><i class="fa fa-edit"></i>&nbsp; Update Faskes - <?= $faskes->nama_faskes;?></li>
    </ol>
  </section>
  <section class="content">
	<div class="row">
	    <div class="col-md-12">
        <?php if(!empty($this->session->flashdata())){ echo $this->session->flashdata('pesan');}?>
	        <div class="box box-primary">
                <div class="box-header with-border">
                </div>
			    <!-- /.box-header -->
			    <div class="box-body">
                    <form action="<?php echo base_url('datafaskes/prosesfaskes');?>" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label>Nama Faskes</label>
                                    <input type="text" class="form-control" value="<?= $faskes->nama_faskes;?>" name="nama_faskes" placeholder="Nama Fasilitas Kesehatan">
                                </div>

                            </div>
                            
                        </div>
                        <div class="pull-right">
                            <input type="hidden" name="edit" value="<?= $faskes->kode_faskes;?>">
                            <button type="submit" class="btn btn-primary btn-md">Edit Data</button> 
                    </form>
                            <a href="<?= base_url('datafaskes');?>" class="btn btn-danger btn-md">Kembali</a>
                        </div>
		        </div>
	        </div>
	    </div>
    </div>
</section>
</div>
