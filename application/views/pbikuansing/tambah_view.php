<?php if(! defined('BASEPATH')) exit('No direct script acess allowed');?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-plus" style="color:green"> </i>  <?= $title_web;?>
    </h1>
    <ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
			<li class="active"><i class="fa fa-plus"></i>&nbsp;  <?= $title_web;?></li>
    </ol>
  </section>
  <section class="content">
	<div class="row">
	    <div class="col-md-12">
	        <div class="box box-primary">
                <div class="box-header with-border">
                </div>
			    <!-- /.box-header -->
			    <div class="box-body">
                    <form action="<?php echo base_url('datakuansing/prosespbikuansing');?>" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6">
								
                                
                                <div class="form-group">
                                    <label>No BPJS</label>
                                    <input type="text" class="form-control" name="no_bpjs"  placeholder="Masukkan Nomor BPJS">
                                </div>
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" class="form-control" name="nik" placeholder="Masukkan Nomor Induk Kependudukan">
                                </div>
                                <div class="form-group">
                                    <label>Nama Peserta</label>
                                    <input type="text" class="form-control" name="nama_peserta" placeholder="Masukkan Nama Peserta PBI">
                                </div>
                                <div class="form-group">
									<label>Nama Faskes Tk. I</label>
									<select class="form-control select2" required="required"  name="faskespilih">
										<option disabled selected value> -- Pilih Faskes Tk.I -- </option>
										<?php foreach($faskes as $isi){?>
											<option value="<?= $isi['kode_faskes'];?>"><?= $isi['nama_faskes'];?></option>
										<?php }?>
									</select>
								</div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control" name="alamat" placeholder="Alamat">
                                </div>

                                <div class="form-group"><label>Status Peserta</label>
											<select name ="status_peserta" class="form-control">
												<option value="AKTIF">AKTIF</option>
												<option value="TIDAK AKTIF">TIDAK AKTIF</option>
											</select>
								</div>
                                
                            </div>

                            
                        </div>
                        <div class="pull-right">
							<input type="hidden" name="tambah" value="tambah">
                            <button type="submit" class="btn btn-primary btn-md">Submit</button> 
                    </form>
                            <a href="<?= base_url('datakuansing');?>" class="btn btn-danger btn-md">Kembali</a>
                        </div>
		        </div>
	        </div>
	    </div>
    </div>
</section>
</div>
