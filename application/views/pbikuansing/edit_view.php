<?php if(! defined('BASEPATH')) exit('No direct script acess allowed');?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-edit" style="color:green"> </i>Update Peserta - <?= $pbikuansing->nama_peserta;?>
    </h1>
    <ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
			<li class="active"><i class="fa fa-edit"></i>&nbsp; Update Peserta - <?= $pbikuansing->nama_peserta;?></li>
    </ol>
  </section>
  <section class="content">
	<div class="row">
	    <div class="col-md-12">
        <?php if(!empty($this->session->flashdata())){ echo $this->session->flashdata('pesan');}?>
	        <div class="box box-primary">
                <div class="box-header with-border">
                </div>
			    <!-- /.box-header -->
			    <div class="box-body">
                    <form action="<?php echo base_url('datakuansing/prosespbikuansing');?>" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6">
								
                               
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" class="form-control" value="<?= $pbikuansing->nik;?>" name="nik" placeholder="NIK">
                                </div>
                                <div class="form-group">
                                    <label>Nama Peserta</label>
                                    <input type="text" class="form-control" value="<?= $pbikuansing->nama_peserta;?>" name="nama_peserta" placeholder="Nama Peserta">
                                </div>
                                <div class="form-group">
									<label>Faskes Tk.I</label>
									<select class="form-control select2" required="required"  name="faskespilih">
										<option disabled selected value> -- Pilih Faskes Tk. I -- </option>
										<?php foreach($faskes as $isi){?>
											<option value="<?= $isi['kode_faskes'];?>" <?php if($isi['kode_faskes'] == $pbikuansing->kode_faskes){ echo 'selected';}?>><?= $isi['nama_faskes'];?></option>
										<?php }?>
									</select>
								</div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control" value="<?= $pbikuansing->alamat;?>" name="alamat" placeholder="Alamat">
                                </div>

                                <div class="form-group">
                                    <label>Status Peserta</label>
										<select name ="status_peserta" class="form-select"  value="<?= $pbikuansing->status_peserta;?>">
                                            <option value="AKTIF" <?php if ($pbikuansing->status_peserta == "AKTIF") echo 'selected';?> >AKTIF</option>
                                            <option value="TIDAK AKTIF" <?php if ($pbikuansing->status_peserta == "TIDAK AKTIF") echo 'selected';?> >TIDAK AKTIF</option>
										</select>
                                </div>
								
                            </div>
                            
                        </div>
                        <div class="pull-right">
                            <input type="hidden" name="edit" value="<?= $pbikuansing->no_bpjs;?>">
                            <button type="submit" class="btn btn-primary btn-md">Edit Data</button> 
                    </form>
                            <a href="<?= base_url('datakuansing');?>" class="btn btn-danger btn-md">Kembali</a>
                        </div>
		        </div>
	        </div>
	    </div>
    </div>
</section>
</div>
